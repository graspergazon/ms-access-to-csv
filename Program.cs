﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Text;

namespace DBReader
{
    class Program
    {
        //private static string ConnectionString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)}; Dbq=D:\\Users\\Kasper\\Code\\AccessDB\\v2.accdb; Uid = Admin;";

        public delegate string GetValueAsString(OdbcDataReader reader, int idx);

        public static string GetStringValue(OdbcDataReader reader, int idx) 
        {
            return reader.GetString(idx);            
        }

        public static string GetIntValue(OdbcDataReader reader, int idx)
        {
            return $"{reader.GetInt32(idx)}";
        }

        public static string GetShortValue(OdbcDataReader reader, int idx)
        {
            return $"{reader.GetInt16(idx)}";
        }

        public static string GetLongValue(OdbcDataReader reader, int idx)
        {
            return $"{reader.GetInt64(idx)}";
        }

        public static string GetFloatValue(OdbcDataReader reader, int idx)
        {
            return $"{reader.GetFloat(idx)}";
        }

        public static string GetDoubleValue(OdbcDataReader reader, int idx)
        {
            return $"{reader.GetFloat(idx)}";
        }

        public static string GetDateTime(OdbcDataReader reader, int idx)
        {
            return $"{reader.GetDateTime(idx).ToString("yyyy-MM-dd")}";
        }


        private static Dictionary<string, GetValueAsString> ConversionMapping;

        static void Main(string[] args)
        {
            string dbFilePath = @"D:\Users\Kasper\Code\AccessDB\v2.accdb";

            if (args.Length > 0) 
            {
                dbFilePath = args[0];
            }

            string connectionString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)}; Dbq=" + dbFilePath + "; Uid = Admin; ";
            var tableNames = GetAllTableNames(connectionString);

            GenerateMappings();

            


            foreach (var tableName in tableNames)
            {
                GenerateCSVForTable(dbFilePath, tableName);
            }

            
        }

        private static List<string> GetAllTableNames(string connectionString)
        {
            List<string> tableNames = new List<string>();
            Console.WriteLine("Searching for tables ...");
            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {

                try
                {

                    connection.Open();

                    System.Data.DataTable tables = connection.GetSchema("tables");

                    foreach (System.Data.DataRow row in tables.Rows)
                    {
                        string name = string.Empty;
                        if (tables.Columns[2].DataType.Equals(typeof(string)))
                        {
                            name = row.ItemArray[2].ToString();

                            
                        }

                        if (tables.Columns[3].DataType.Equals(typeof(string))) 
                        {
                            string tableType = row.ItemArray[3].ToString();

                            if (tableType.Equals("TABLE")) 
                            {
                                if (!string.IsNullOrEmpty(name)) 
                                {
                                    tableNames.Add(name);

                                    Console.WriteLine($"Found table {name}");
                                }
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            Console.WriteLine("Done\n\n");

            return tableNames;
        }
        private static void GenerateMappings() 
        {
            //for all data types check https://docs.microsoft.com/en-us/sql/odbc/microsoft/microsoft-access-data-types?view=sql-server-ver15

            ConversionMapping = new Dictionary<string, GetValueAsString>();
            ConversionMapping.Add("VARCHAR", GetStringValue);
            ConversionMapping.Add("LONGCHAR", GetStringValue);
            ConversionMapping.Add("COUNTER", GetIntValue);
            ConversionMapping.Add("INTEGER", GetIntValue);
            //ConversionMapping.Add("SHORT", GetShortValue);
            //ConversionMapping.Add("LONG", GetLongValue);
            //ConversionMapping.Add("SINGLE", GetFloatValue);
            //ConversionMapping.Add("DOUBLE", GetDoubleValue);
            //ConversionMapping.Add("VARCHAR", GetStringValue);

            ConversionMapping.Add("DATETIME", GetDateTime);
        }

        
        private static void GenerateCSVForTable(string dbFilePath, string table)
        {
            string connectionString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)}; Dbq=" + dbFilePath + "; Uid = Admin; ";

            string query = $"SELECT * FROM {table}";
            Console.WriteLine($"\nGetting data from table {table}:");
            OdbcCommand command = new OdbcCommand(query);

            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {
                
                try
                {
                    command.Connection = connection;
                    connection.Open(); 


                    string firstRow = string.Empty;
                    using (var reader = command.ExecuteReader())
                    {
                        int row = 0;
                        StringBuilder sb = new StringBuilder();                       
                       
                        while (reader.Read())
                        {
                            

                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                string name = reader.GetName(i);
                                string dataType = reader.GetDataTypeName(i);

                                

                                string valueString = " ";

                                if (reader.IsDBNull(i))
                                {
                                    valueString = " ";
                                }
                                else if (ConversionMapping.ContainsKey(dataType))
                                {
                                     valueString = ConversionMapping[dataType](reader, i);
                                }
                                else
                                {
                                    var data = reader.GetValue(i);

                                    valueString = data.ToString();
                                    //DateTime test = reader.GetDateTime(i);
                                }

                                if (row == 0)
                                {
                                    if (i < reader.FieldCount - 1)
                                    {
                                        sb.Append($"{name}, ");
                                        firstRow += $"{valueString}, ";
                                    }
                                    else
                                    {
                                        sb.Append($"{name}\n");
                                        firstRow += $"{valueString}";
                                    }

                                    Console.WriteLine($"Column {i}, name {name}, data type {dataType} ");

                                }
                                else 
                                {
                                    if (i < reader.FieldCount - 1)
                                    {
                                        sb.Append($"{valueString}, ");
                                    }
                                    else
                                    {
                                        sb.Append($"{valueString}\n");                                        
                                    }
                                }
                               
                            }

                            if (row == 0)
                                sb.AppendLine(firstRow);

                            row++;
                        }

                        Console.WriteLine($"{row} rows\n ");

                        string outputDir = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(dbFilePath), System.IO.Path.GetFileNameWithoutExtension(dbFilePath));

                        if (!System.IO.Directory.Exists(outputDir)) 
                        {
                            System.IO.Directory.CreateDirectory(outputDir);
                        }

                        string csvFilename = System.IO.Path.Combine(outputDir, $"{table}.csv");

                        Console.WriteLine($"Writing csv data to {csvFilename}");

                        bool writeCSV = true;
                        if (System.IO.File.Exists(csvFilename)) 
                        {
                            Console.WriteLine($"\n{csvFilename} already exists. \nOverwrite y/n?");

                            string response = Console.ReadLine();
                            if (response.ToLower().Contains("n"))
                                writeCSV = false;
                        }

                        if(writeCSV)
                            System.IO.File.WriteAllText(csvFilename, sb.ToString());

                        //Console.WriteLine(sb.ToString());

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Could not open table {table}: {ex.Message}");
                }
            }

        }
    }
}
