Simple tool to convert tables from an MicroSoft Access DB file into separate CSV files

### Howto build
Download .NET 5 SDK from [https://dotnet.microsoft.com/en-us/download/dotnet/5.0](https://dotnet.microsoft.com/en-us/download/dotnet/5.0) and install  
Optional: Install VS Code and add C# Extension.  
dotnet restore  
dotnet run <full path to .accdb file>

